#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    FILE *s;
    s = fopen("homes200.csv", "r");
    if (s == NULL)
    {
        printf("Can't open homes200.csv file\n");
        exit(1);
    }
    
    char address[30];
    int zip, ID, addr, NumCama, NumBano, area, price, count =0;
    double biggest_price=0, lowest_price=10000000000000, average;
    char biggest_state[20];
    int test =0;
    while (fscanf(s, "%d,%d,%[^,],%d,%d,%d,%d", &zip, &ID, address, &price, &NumCama, &NumBano, &area) != EOF)
    {
        average = average +price;
        count +=1;
        if(biggest_price<price)
        {
            biggest_price=price;
        }
        
        if(lowest_price>price)
        {
            lowest_price = price;
        }
    }
    average = average/count;
    fclose(s);
    printf("%f %f %f", biggest_price, lowest_price, average);
}
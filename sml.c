#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    FILE *s;
    s = fopen("homelistings.csv", "r");
    if (s == NULL)
    {
        printf("Can't open states.csv file\n");
        exit(1);
    }
    
    FILE *large;
    large = fopen("large.txt", "w");
    if (!large)
    {
        printf("Can't open big.txt\n");
        exit(1);
    }
    FILE *med;
    med = fopen("med.txt", "w");
    if (!large)
    {
        printf("Can't open med.txt\n");
        exit(1);
    }
    
    FILE *small;
    small = fopen("small.txt", "w");
    if (!small)
    {
        printf("Can't open small.txt\n");
        exit(1);
    }
    
    char address[30];
    int zip, ID, addr, NumCama, NumBano, area, price;
    double biggest_density = 0;
    char biggest_state[20];
    while (fscanf(s, "%d,%d,%[^,],%d,%d,%d,%d", &zip, &ID, address, &price, &NumCama, &NumBano, &area) != EOF)
    {
        if (area > 2000)
        {
            fprintf(large, "%s\n", address);
        }
        else if (area<=2000 && area>=1000)
        {
            fprintf(med, "%s\n", address);
        }
        else
        {
             fprintf(small, "%s\n", address);
        }
        
    }
    fclose(s);
    fclose(large);
    fclose(med);
    fclose(small);
    
}
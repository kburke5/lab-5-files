#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    FILE *s;
    s = fopen("homelistings.csv", "r");
    if (s == NULL)
    {
        printf("Can't open states.csv file\n");
        exit(1);
    }
    
    FILE *large;
    large = fopen("large.txt", "w");
    if (!large)
    {
        printf("Can't open big.txt\n");
        exit(1);
    }
    
    char address[30], zipC[13], PrimeroZip[13];
    int zip, ID, addr, NumCama, NumBano, area, price;
    double biggest_density = 0;
    char biggest_state[20];
    fscanf(s, "%d,%d,%[^,],%d,%d,%d,%d", &zip, &ID, address, &price, &NumCama, &NumBano, &area);
    sprintf(PrimeroZip, "%d.txt", zip);
    while (fscanf(s, "%d,%d,%[^,],%d,%d,%d,%d", &zip, &ID, address, &price, &NumCama, &NumBano, &area) != EOF)
    {

            sprintf(zipC, "%d.txt", zip);
            if(zipC[4] == PrimeroZip[4] && zipC[3] == PrimeroZip[3] && zipC[2] == PrimeroZip[2])
            {
                large=fopen(PrimeroZip,"a");
                fprintf(large,"%s\n\r", address);
            }
            else
            {
                large=fopen(zipC, "a");
                fprintf(large,"%s\n\r", address);
        }
        }
    fclose(s);
    fclose(large);
}